use std::path::PathBuf;

use crate::loadable::JsonFile;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WebhookConfigMessages {
    pub archive_start: String,
    pub download_start: String,
    pub download_finish: String,
    pub archive_finish: String,
    pub no_new_file_found: String,
    pub new_file_found_header: String,
    pub postfind_script_start: String,
    pub postfind_script_progress: String,
    pub postfind_script_end_error: String,
    pub postfind_script_end_error_exitcode: String,
    pub postfind_script_end_ok: String,
    pub error_suffix: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WebhookConfig {
    pub url: String,
    pub messages: WebhookConfigMessages,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub webhook: WebhookConfig,
    pub schedule_interval_seconds: Option<u64>,
    pub postfind_script: PathBuf,
    pub postfind_script_progress_filename: PathBuf,
}

impl JsonFile for Config {}

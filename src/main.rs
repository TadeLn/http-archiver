pub mod archive;
pub mod config;
pub mod loadable;
pub mod webhook;
pub mod error;
pub mod headers;

use crate::archive::ResponseInfo;
use archive::ArchiveInfo;
use chrono::{DateTime, SecondsFormat, Utc};
use config::{Config, WebhookConfigMessages};
use error::AppError;
use loadable::{JsonFile, LoadError};
use headers::{headers_to_map, headers_to_vec};
use std::{
    env::args, fs, path::Path, process::Command, sync::{Arc, Mutex}, thread::{self}, time::{Duration, SystemTime}
};
use webhook::{error::WebhookError, Webhook};


fn read_archive_info_from_file_or_default(archive_info_path: &Path) -> Result<ArchiveInfo, LoadError> {
    match ArchiveInfo::from_file(archive_info_path) {
        Ok(a) => Ok(a),
        Err(LoadError::IoError(_)) => Ok(ArchiveInfo::default()),
        Err(e) => Err(e),
    }
}

fn current_timestamp_in_seconds_or_0() -> u64 {
    SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap_or(Duration::ZERO).as_secs()
}

fn get_schedule_info_string(start_timestamp: u64, schedule_interval_seconds: Option<u64>) -> String {
    if let Some(schedule_interval) = schedule_interval_seconds {
        let next_check_timestamp = start_timestamp + schedule_interval;
        format!(" (next check scheduled <t:{next_check_timestamp}:R>)")
    } else {
        String::new()
    }
}

struct ArchiverResult {
    new: bool,
    response_text: String,
    response_info: ResponseInfo,
}

fn start_archiver(url: &str, webhook: &mut Webhook, msg: &WebhookConfigMessages) -> Result<ArchiverResult, AppError> {
    webhook.try_set_status_message(&msg.archive_start);

    let archive_dir_path = Path::new("archive");
    let archive_files_dir_path = &archive_dir_path.join("files");
    fs::create_dir_all(archive_files_dir_path)?;

    let archive_info_path = archive_dir_path.join("archive_info.json");
    let mut archive_info = read_archive_info_from_file_or_default(&archive_info_path)?;

    let timestamp = current_timestamp_in_seconds_or_0();
    let datetime = DateTime::from_timestamp(timestamp as i64, 0).ok_or(AppError::DateTimeError)?;

    webhook.try_set_status_message(&msg.download_start);

    let response = reqwest::blocking::get(url)?;
    let status = response.status();
    let (headers_map, headers_map_imperfect) = headers_to_map( response.headers());
    let headers_list = if headers_map_imperfect {
        Some(headers_to_vec( response.headers()))
    } else {
        None
    };
    let response_text = response.text()?;

    println!("{}", response_text);

    webhook.try_set_status_message(&msg.download_finish);

    fs::write(archive_dir_path.join("latest_response.txt"), &response_text)?;

    let md5_digest = md5::compute(&response_text);
    let md5_hash = format!("{md5_digest:x}");
    let new = archive_info.is_new(&md5_hash);

    let response_file_path = archive_files_dir_path.join(format!("{md5_hash}.txt"));
    fs::write(&response_file_path, &response_text)?;

    let response_info = ResponseInfo {
        filename: response_file_path.to_str().unwrap_or_default().to_string(),
        url: url.to_owned(),
        md5_hash: md5_hash.clone(),
        status_code: status.as_u16(),
        success: status.is_success(),
        headers: headers_list,
        headers_map: Some(headers_map),
        timestamp: timestamp,
        timestamp_string: datetime.format("%Y-%m-%dT%H:%M:%S").to_string(),
    };

    archive_info.add_response(response_info.clone());
    archive_info.to_file(&archive_info_path, true)?;

    webhook.try_set_status_message(&msg.archive_finish);

    Ok(ArchiverResult {
        new,
        response_text,
        response_info,
    })
}

fn start_post_find_script(config: &Config, webhook: &mut Webhook, msg: &WebhookConfigMessages, start_timestamp: u64) -> Result<(), AppError> {
    webhook.try_set_status_message(&msg.postfind_script_start);

    let script_result = Arc::new(Mutex::new(None));

    {
        // Launch script
        let script_result_clone = script_result.clone();
        let postfind_script_name = config.postfind_script.clone();
        thread::spawn(move || {
            let mut command = Command::new(postfind_script_name);
            let command_result = command.output();
            let mut guard = script_result_clone.lock().unwrap();
            *guard = Some(command_result);
        });
    }

    // Sleep for shorter for the first time to get a quicker first update
    thread::sleep(Duration::from_secs(3));

    loop {
        {
            // Check if script has finished
            let guard = script_result.lock().unwrap();
            if let Some(result) = &*guard {
                match result {
                    Ok(output) => {
                        if output.status.success() {
                            // Successful finish
                            let content = &msg.postfind_script_end_ok;
                            let schedule_info = get_schedule_info_string(start_timestamp, config.schedule_interval_seconds);
                            webhook.try_set_status_success_message(&format!("{content}{schedule_info}"));
                        } else {
                            // Error finish
                            let exit_code_string = &output.status.code().map_or("none".to_string(), |x| x.to_string());
                            let content = &msg.postfind_script_end_error_exitcode.replace("$?", &exit_code_string);
                            webhook.try_send_error_message(&content, &msg.error_suffix);
                        }
                    }
                    Err(e) => {
                        // Error launch
                        eprintln!("[postfind-script] launch error: {:#?}", e);
                        webhook.try_send_error_message(&msg.postfind_script_end_error, &msg.error_suffix);
                    }
                };

                break;
            } else {
                // Script is not finished yet, send progress update
                let current_progress =
                    fs::read_to_string(&config.postfind_script_progress_filename).unwrap_or("*Warning: progress file could not be read*".to_string());

                let message = &msg.postfind_script_progress;
                let next_update_timestamp = current_timestamp_in_seconds_or_0() + 11;
                let content = format!("{message}{current_progress} (next update <t:{next_update_timestamp}:R>)");
                webhook.try_set_status_message(&content);
                eprintln!("[postfind-script] progress: {content}");
            }
        }
        // Let guard go out of scope before sleeping

        thread::sleep(Duration::from_secs(10));
    }

    Ok(())
}

fn run_main_process(
    url: &str,
    force_postfind_script: bool,
    config: &Config,
    webhook: &mut Webhook,
    msg: &WebhookConfigMessages,
    start_timestamp: u64,
) -> Result<(), AppError> {
    let ArchiverResult {
        new,
        response_info,
        response_text,
    } = start_archiver(url, webhook, msg)?;

    // Notify about new file found
    if new {
        let header = &msg.new_file_found_header;
        let timestamp = response_info.timestamp;
        let md5_hash = response_info.md5_hash;
        webhook.send_new_message(&format!(
            "{header}\n```json\n{response_text}\n```\nArchived at: <t:{timestamp}>\nMD5 hash: `{md5_hash}`",
        ))?;
    }

    // Launch the post-find script after finding a new file
    if new || force_postfind_script {
        start_post_find_script(config, webhook, msg, start_timestamp)?;
    } else {
        let content = &msg.no_new_file_found;
        let schedule_info = get_schedule_info_string(start_timestamp, config.schedule_interval_seconds);
        webhook.set_status_success_message(&format!("{content}{schedule_info}"))?;
    }
    Ok(())
}

fn launch_with_webhook(url: &str, force_postfind_script: bool, start_timestamp: u64) -> Result<(), WebhookError> {
    let current_datetime = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    println!("================================");
    println!("http-archiver started at {current_datetime}");
    eprintln!("================================");
    eprintln!("http-archiver started at {current_datetime}");

    // Load webhook configuration
    let config = Config::from_file(Path::new("config.json")).expect("Could not load 'config.json' - please fix this immediately!");
    let mut webhook = Webhook::from_config(&config);
    let msg = &webhook.webhook_config.messages.clone();

    // Handle all app errors with webhook
    match run_main_process(url, force_postfind_script, &config, &mut webhook, msg, start_timestamp) {
        Ok(()) => {}
        Err(e) => {
            eprintln!("fatal error: {:#?}", e);
            webhook.send_error_message("App crashed! Check logs for details", &msg.error_suffix)?;
        }
    }
    Ok(())
}

fn launch_debug() -> Result<(), WebhookError> {
    let config = Config::from_file(Path::new("config_debug.json")).expect("Could not load 'config_debug.json' - please fix this immediately!");

    let mut webhook = Webhook::from_config(&config);
    let msg = &webhook.webhook_config.messages.clone();

    webhook.set_status_message("A")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("B")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("C")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.send_error_message("Test error message", &msg.error_suffix)?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("D")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("E")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("F")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.send_new_message("Test normal message")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("G")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("H")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_message("I")?;
    thread::sleep(Duration::from_millis(1000));
    webhook.set_status_success_message("Test finished")?;
    Ok(())
}

fn main() -> Result<(), AppError> {
    let start_timestamp = current_timestamp_in_seconds_or_0();
    let args: Vec<String> = args().collect();

    if args.len() < 2 {
        eprintln!("usage: {} <url_to_download> [\"force\"]", args[0]);
    } else {
        if args[1] == "debug" {
            launch_debug()?;
        } else {
            let force = {
                if let Some(force_arg) = args.get(2) {
                    force_arg == "force"
                } else {
                    false
                }
            };
            launch_with_webhook(&args[1], force, start_timestamp)?;
        }
    }
    Ok(())
}

use std::io;

use crate::{loadable::LoadError, webhook::error::WebhookError};


#[derive(Debug)]
pub enum AppError {
    #[allow(dead_code)]
    ReqwestError(reqwest::Error),
    #[allow(dead_code)]
    IoError(io::Error),
    #[allow(dead_code)]
    LoadError(LoadError),
    #[allow(dead_code)]
    WebhookError(WebhookError),
    DateTimeError,
}

impl From<reqwest::Error> for AppError {
    fn from(value: reqwest::Error) -> Self {
        AppError::ReqwestError(value)
    }
}
impl From<io::Error> for AppError {
    fn from(value: io::Error) -> Self {
        AppError::IoError(value)
    }
}
impl From<LoadError> for AppError {
    fn from(value: LoadError) -> Self {
        AppError::LoadError(value)
    }
}
impl From<WebhookError> for AppError {
    fn from(value: WebhookError) -> Self {
        AppError::WebhookError(value)
    }
}
use crate::{
    config::{Config, WebhookConfig},
    current_timestamp_in_seconds_or_0,
};
use error::{log_and_ignore, WebhookError};
use request::{WebhookEditMessageRequest, WebhookSendMessageRequest};
use response::DiscordMessage;
use std::fs;

pub mod error;
pub mod request;
pub mod response;

pub struct Webhook {
    status_message_id: Option<u64>,
    pub webhook_config: WebhookConfig,
}

const STATUS_MESSAGE_ID_FILENAME: &str = "status_message_id.txt";

impl Webhook {
    fn read_status_message_id_from_file() -> Option<u64> {
        match fs::read_to_string(STATUS_MESSAGE_ID_FILENAME) {
            Ok(text) => match text.parse::<u64>() {
                Ok(num) => Some(num),
                Err(_) => None,
            },
            Err(_) => None,
        }
    }

    fn try_write_status_message_id_to_file(status_message_id: Option<u64>) {
        let text = match status_message_id {
            Some(num) => format!("{num}"),
            None => String::new(),
        };
        let _ = fs::write(STATUS_MESSAGE_ID_FILENAME, text);
    }

    pub fn from_config(config: &Config) -> Self {
        let id = Self::read_status_message_id_from_file();

        Self {
            webhook_config: config.webhook.clone(),
            status_message_id: id,
        }
    }

    fn send_message(&mut self, content: String) -> Result<DiscordMessage, WebhookError> {
        let req = WebhookSendMessageRequest { content };
        req.send_using_webhook_url(&self.webhook_config.url)
    }

    fn edit_message(&self, message_id: u64, content: String) -> Result<DiscordMessage, WebhookError> {
        let req = WebhookEditMessageRequest { message_id, content };
        req.send_using_webhook_url(&self.webhook_config.url)
    }

    fn edit_or_send_status_message(&mut self, content: String) -> Result<(), WebhookError> {
        if let Some(message_id) = self.status_message_id {
            // There is an active status message, try to edit it
            let result = self.edit_message(message_id, content.clone());
            if result.is_ok() {
                // If edited successfully, exit out of the function
                return Ok(());
            }

            // If not, log the error and send a normal message
            log_and_ignore(result);
        }

        // There is no active status message or cannot edit status message: create a new one
        let discord_message = self.send_message(content)?;

        self.status_message_id = Some(discord_message.id_u64()?);
        Self::try_write_status_message_id_to_file(self.status_message_id);
        Ok(())
    }

    pub fn send_new_message(&mut self, content: &str) -> Result<(), WebhookError> {
        self.send_message(content.to_owned())?;
        self.status_message_id = None;
        Self::try_write_status_message_id_to_file(self.status_message_id);
        Ok(())
    }

    pub fn send_error_message(&mut self, error_message: &str, error_suffix: &str) -> Result<(), WebhookError> {
        let timestamp = current_timestamp_in_seconds_or_0();
        self.send_new_message(&format!("<t:{timestamp}:t> :warning: **Error:** {error_message}{error_suffix}"))
    }

    pub fn set_status_message(&mut self, status_message: &str) -> Result<(), WebhookError> {
        let timestamp = current_timestamp_in_seconds_or_0();
        self.edit_or_send_status_message(format!("<t:{timestamp}:t> :information_source: **Status:** {status_message}"))
    }

    pub fn set_status_success_message(&mut self, status_message: &str) -> Result<(), WebhookError> {
        let timestamp = current_timestamp_in_seconds_or_0();
        self.edit_or_send_status_message(format!("<t:{timestamp}:t> :white_check_mark: **Success:** {status_message}"))
    }

    // If it fails, it logs the result to a file and ignores the error
    pub fn try_send_new_message(&mut self, content: &str) {
        log_and_ignore(self.send_new_message(content));
    }

    // If it fails, it logs the result to a file and ignores the error
    pub fn try_send_error_message(&mut self, error_message: &str, error_suffix: &str) {
        log_and_ignore(self.send_error_message(error_message, error_suffix));
    }

    // If it fails, it logs the result to a file and ignores the error
    pub fn try_set_status_message(&mut self, status_message: &str) {
        log_and_ignore(self.set_status_message(status_message));
    }

    // If it fails, it logs the result to a file and ignores the error
    pub fn try_set_status_success_message(&mut self, status_message: &str) {
        log_and_ignore(self.set_status_success_message(status_message));
    }
}

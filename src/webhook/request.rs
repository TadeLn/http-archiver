use super::{
    error::WebhookError,
    response::{WebhookEditMessageResponse, WebhookSendMessageResponse},
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct WebhookSendMessageRequest {
    pub content: String,
}

impl WebhookSendMessageRequest {
    pub fn send_using_webhook_url(&self, webhook_url: &str) -> Result<WebhookSendMessageResponse, WebhookError> {
        let req_body = serde_json::to_string(&self)?;
        eprintln!("[webhook] send message request: {req_body}");

        let client = reqwest::blocking::Client::new();
        let result = client
            .post(format!("{webhook_url}?wait=true"))
            .body(req_body)
            .header("Content-Type", "application/json")
            .send();

        match result {
            Ok(res) => {
                eprintln!("[webhook] send message response: ok");
                let res_body = res.text()?;
                let parsed_response = serde_json::from_str::<WebhookSendMessageResponse>(&res_body)?;
                Ok(parsed_response)
            }
            Err(error) => {
                eprintln!("[webhook] send message error: {error:#?}");
                Err(WebhookError::ReqwestError(error))
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct WebhookEditMessageRequest {
    pub message_id: u64,
    pub content: String,
}

impl WebhookEditMessageRequest {
    pub fn send_using_webhook_url(&self, webhook_url: &str) -> Result<WebhookEditMessageResponse, WebhookError> {
        let req_body = serde_json::to_string(&self)?;
        let message_id = self.message_id;
        eprintln!("[webhook] edit message request: {req_body}");

        let client = reqwest::blocking::Client::new();
        let result = client
            .patch(format!("{webhook_url}/messages/{message_id}"))
            .body(req_body)
            .header("Content-Type", "application/json")
            .send();

        match result {
            Ok(res) => {
                eprintln!("[webhook] edit message response: ok");
                let res_body = res.text()?;
                let parsed_response = serde_json::from_str::<WebhookEditMessageResponse>(&res_body)?;
                Ok(parsed_response)
            }
            Err(error) => {
                eprintln!("[webhook] edit message error: {error:#?}");
                Err(WebhookError::ReqwestError(error))
            }
        }
    }
}

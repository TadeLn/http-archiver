use serde::{Deserialize, Serialize};
use std::num::ParseIntError;

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscordMessage {
    pub id: String,
}

impl DiscordMessage {
    pub fn id_u64(&self) -> Result<u64, ParseIntError> {
        self.id.parse::<u64>()
    }
}

pub type WebhookSendMessageResponse = DiscordMessage;
pub type WebhookEditMessageResponse = DiscordMessage;

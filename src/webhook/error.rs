use std::num::ParseIntError;

#[derive(Debug)]
pub enum WebhookError {
    SerdeJsonError(serde_json::Error),
    ReqwestError(reqwest::Error),
    ParseIntError(ParseIntError),
}

impl From<serde_json::Error> for WebhookError {
    fn from(value: serde_json::Error) -> Self {
        Self::SerdeJsonError(value)
    }
}

impl From<reqwest::Error> for WebhookError {
    fn from(value: reqwest::Error) -> Self {
        Self::ReqwestError(value)
    }
}

impl From<ParseIntError> for WebhookError {
    fn from(value: ParseIntError) -> Self {
        Self::ParseIntError(value)
    }
}

pub fn log_and_ignore<T>(result: Result<T, WebhookError>) {
    match result {
        Ok(_) => {}
        Err(e) => eprintln!("warning: webhook broke: {e:?}"),
    };
}

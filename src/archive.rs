
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use crate::{loadable::JsonFile, headers::StringOrBytes, };

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ResponseInfo {
    pub filename: String,
    pub url: String,
    pub status_code: u16,
    pub success: bool,
    pub headers: Option<Vec<(String, StringOrBytes)>>,
    pub headers_map: Option<IndexMap<String, StringOrBytes>>,
    pub md5_hash: String,
    pub timestamp: u64,
    #[serde(default)]
    pub timestamp_string: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileInfo {
    pub filename: String,
    pub url: String,
    pub md5_hash: String,
    pub timestamp: u64,
    #[serde(default)]
    pub timestamp_string: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct ArchiveInfo {
    #[serde(default)]
    pub last_check_timestamp: u64,
    #[serde(default)]
    pub responses: Vec<ResponseInfo>,
    #[serde(default)]
    pub files: Vec<FileInfo>,
}

impl ArchiveInfo {
    pub fn add_response(&mut self, response_info: ResponseInfo) {
        self.last_check_timestamp = response_info.timestamp;

        if self.files.iter().find(|x| x.md5_hash == response_info.md5_hash).is_none() {
            self.files.push(FileInfo {
                filename: response_info.filename.clone(),
                url: response_info.url.clone(),
                md5_hash: response_info.md5_hash.clone(),
                timestamp: response_info.timestamp,
                timestamp_string: response_info.timestamp_string.clone(),
            })
        }

        self.responses.push(response_info);
    }

    pub fn is_new(&self, md5_hash: &str) -> bool {
        self.files.iter().find(|x| x.md5_hash == md5_hash).is_none()
    }
}

impl JsonFile for ArchiveInfo {}

use indexmap::IndexMap;
use reqwest::header::{HeaderMap, HeaderValue};
use serde::{Deserialize, Serialize};


#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub enum StringOrBytes {
    String(String),
    Bytes(Vec<u8>),
}

impl From<&HeaderValue> for StringOrBytes {
    fn from(header_value: &HeaderValue) -> Self {
        if let Ok(text) = header_value.to_str() {
            Self::String(text.to_owned())
        } else {
            Self::Bytes(Vec::from(header_value.as_bytes()))
        }
    }
}

pub fn headers_to_vec(header_map: &HeaderMap) -> Vec<(String, StringOrBytes)> {
    let mut pairs = Vec::new();
    for (header_name, header_value) in header_map {
        pairs.push((header_name.to_string(), header_value.into()));
    }
    return pairs;
}


pub fn headers_to_map(header_map: &HeaderMap) -> (IndexMap<String, StringOrBytes>, bool) {
    let mut map = IndexMap::new();
    let mut imperfect = false;
    for (header_name, header_value) in header_map {
        let old_value = map.insert(header_name.to_string().to_lowercase(), header_value.into());
        if old_value.is_some() {
            imperfect = true;
        }
    }
    (map, imperfect)
}
